package com.slylamb.quickphrase;

public enum EnumLanguage {
    maori,
    french,
    german,
    dutch,
    italian,
    spanish,
    portuguese,
    mandarin,
    japanese,
    xhosa,
    none
}

