/**
 * Assignment 3
 * Shelton Lai Lam
 * ID: 17046357
 *
 * The following app is a basic language translator for everyday phrases from English
 * to a selection of 10 languages (refer to EnumLanguage java file for list).
 * There are currently 14 English phrases that can be played as audio files to the user
 * based on the user's selected translation language.
 *
 * Note: the buttons have a property: backgroundTint which only works in API 21 and higher,
 * this does not cause any errors and is ignored by lower API levels
 *
 * For conciseness and expandability purposes, the app does not have set methods for every button
 * and it also does not have language classes with set audio resource ID references/fields.
 *
 * To add more languages to the app, update:
 * - the Enum class
 * - the Menu and menu item
 * - the onOptionsItemSelected
 * - adding the resource files to app/res/raw in the format language_ + button_id_name.m4a
 * No need to update methods for playing new audio resources
 *
 * To add more phrases to the app, update:
 * - the new button(s) in activity_main with IDs in the format audio_file_name without the language or .m4a
 * - the resources for every translation language in the Enum class
 */

package com.slylamb.quickphrase;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.media.MediaPlayer;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

    private static String TAG = "DEBUG_LOG";        //< used for Logcat filtering
    private static String LANGUAGE = "language";    //< key for storing mLanguage
    private static String HEADER = "header";        //< key for storing mHeader text
    private EnumLanguage mLanguage;             //< the current translation language
    private MediaPlayer mPlayer;                //< media player of R.raw audio resources
    private TextView mHeader;                   //< the main header text view

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mHeader = (TextView) findViewById(R.id.txt_header);

        // Reclaim resources from a saved instance state
        if(savedInstanceState != null){
            mLanguage = (EnumLanguage) savedInstanceState.get(LANGUAGE);
            String headerText = (String) savedInstanceState.get(HEADER);

            // set mHeader to display current translation language
            mHeader.setText(headerText);
            Log.i(TAG,"Saved resources reclaimed");
        }else{
            mLanguage = EnumLanguage.none;
            // set mHeader to default header
            mHeader.setText(R.string.header);
        }

    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState){
        Log.i(TAG,"Saving Instant State");
        super.onSaveInstanceState(savedInstanceState);
        // store the current translation language
        savedInstanceState.putSerializable(LANGUAGE, mLanguage);

        // store the current header text
        savedInstanceState.putString(HEADER,mHeader.getText().toString());
    }

    /**
     * determines which button was clicked and plays the corresponding audio file
     * based on the current translation language
     * @param view the english phrase button clicked
     */
    public void btnClicked(View view){
        // exit method if no language was selected
        if(mLanguage == EnumLanguage.none){
            // notify user to make a translation language selection from the menu bar first
            Toast.makeText(this,"Select a language from the menu bar",
                    Toast.LENGTH_SHORT).show();
            return;
        }

        // create audio file prefix
        String prefix = mLanguage.toString() + "_";

        // get the clicked view id number
        int viewID = view.getId();
        Log.i(TAG, "Button click start " + viewID);

        // get the clicked view name given in the activity_main layout
        String givenID = view.getResources().getResourceEntryName(viewID);
        Log.i(TAG, "Button click start " + prefix + givenID);

        // get the audio file id number: prefix + givenID = audio resource name
        int audioID = getResources().getIdentifier(prefix + givenID, "raw", "com.slylamb.quickphrase");
        Log.i(TAG, "Button click start " + audioID);

        // play the audio file
        mPlayer = MediaPlayer.create(this, audioID);
        mPlayer.start();

        // this is necessary to ensure release is only called when the
        // Media Player is stopped and in the appropriate state
        while(mPlayer.isPlaying()){
            if(!mPlayer.isPlaying()){mPlayer.stop();}
        }

        // release and nullify Media Player
        // this causes major errors if not done
        mPlayer.release();
        mPlayer = null;
    }

    // creates the translation language menu
    @Override
    public boolean onCreateOptionsMenu(Menu m) {
        getMenuInflater().inflate(R.menu.menu,m);
        return true;
    }

    /**
     * returns true if a translation language is selected, otherwise false
     * @param item the translation menu item
     * @return the success or failure of item selection by user
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        // set mLanguage to the translation language selected
        // set change mHeader to display the current language or default R.string.header
        switch (item.getItemId()) {
            case R.id.mItem_maori:
                mLanguage = EnumLanguage.maori;
                mHeader.setText(R.string.maori);
                Log.i(TAG, mLanguage.toString() + " selected.");
                return true;
            case R.id.mItem_french:
                mLanguage = EnumLanguage.french;
                mHeader.setText(R.string.french);
                Log.i(TAG, mLanguage.toString() + " selected.");
                return true;
            case R.id.mItem_german:
                mLanguage = EnumLanguage.german;
                mHeader.setText(R.string.german);
                Log.i(TAG, mLanguage.toString() + " selected.");
                return true;
            case R.id.mItem_dutch:
                mLanguage = EnumLanguage.dutch;
                mHeader.setText(R.string.dutch);
                Log.i(TAG, mLanguage.toString() + " selected.");
                return true;
            case R.id.mItem_italian:
                mLanguage = EnumLanguage.italian;
                mHeader.setText(R.string.italian);
                Log.i(TAG, mLanguage.toString() + " selected.");
                return true;
            case R.id.mItem_spanish:
                mLanguage = EnumLanguage.spanish;
                mHeader.setText(R.string.spanish);
                Log.i(TAG, mLanguage.toString() + " selected.");
                return true;
            case R.id.mItem_portuguese:
                mLanguage = EnumLanguage.portuguese;
                mHeader.setText(R.string.portuguese);
                Log.i(TAG, mLanguage.toString() + " selected.");
                return true;
            case R.id.mItem_mandarin:
                mLanguage = EnumLanguage.mandarin;
                mHeader.setText(R.string.mandarin);
                Log.i(TAG, mLanguage.toString() + " selected.");
                return true;
            case R.id.mItem_japanese:
                mLanguage = EnumLanguage.japanese;
                mHeader.setText(R.string.japanese);
                Log.i(TAG, mLanguage.toString() + " selected.");
                return true;
            case R.id.mItem_xhosa:
                mLanguage = EnumLanguage.xhosa;
                mHeader.setText(R.string.xhosa);
                Log.i(TAG, mLanguage.toString() + " selected.");
                return true;
            case R.id.mItem_none:
                mLanguage = EnumLanguage.none;
                mHeader.setText(R.string.header);
                Log.i(TAG, mLanguage.toString() + " selected.");
                return true;
            default:
                mLanguage = EnumLanguage.none;
                mHeader.setText(R.string.header);
                Log.i(TAG, mLanguage.toString() + " selected.");
                return false;
        }
    }

}
